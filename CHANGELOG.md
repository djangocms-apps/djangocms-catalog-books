# Changelog
All notable changes to this project are documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.1] - 2025-01-09

### Fixed
- Fix url to book by name.

## [1.2.0] - 2025-01-09

### Fixed
- Get detail of book by pk.

### Added
- Get detail by book name.


## [1.1.0] - 2024-10-03

- Add field Coming soon.

## [1.0.0] - 2024-04-12

### Added
- Catalog of book.


[Unreleased]: https://gitlab.nic.cz/djangocms-apps/djangocms-catalog-books/-/compare/1.2.1...main
[1.2.1]: https://gitlab.nic.cz/djangocms-apps/djangocms-catalog-books/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.nic.cz/djangocms-apps/djangocms-catalog-books/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.nic.cz/djangocms-apps/djangocms-catalog-books/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.nic.cz/djangocms-apps/djangocms-catalog-books/-/compare/e3230af71b78e79d...1.0.0
