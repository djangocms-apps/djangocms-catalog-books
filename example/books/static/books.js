/*
    Books example.
*/

function setOrderBy() {
    const element = document.getElementById("sort-by")
    if (element) {
        element.addEventListener("change", function (event) {
            const path = event.target.querySelector(`option[value=${event.target.value}]`).dataset.url
            const url = new URL(`${window.location.protocol}//${window.location.host}${path}`)
            url.searchParams.set(event.target.name, event.target.value)
            window.location.replace(url)
        })
    }
}

function collapseSelectedFilters() {
    document.querySelectorAll('.catalog-books-filter input[type=checkbox]:checked').forEach(function (node) {
        const header = node.closest('ul').parentElement.previousElementSibling
        if (header.classList.contains('collapsed')) {
            header.click()
        }
    })
}

document.addEventListener("DOMContentLoaded", function () {
    setOrderBy()
    collapseSelectedFilters()
})
